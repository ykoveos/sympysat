import os
import math
import numpy as np
import matplotlib.pyplot as plt

from qb import qb
from qb import figPlotter

import importlib
from os.path import split

# importlib.reload(qb) # does not reload plotter
# importlib.reload(qb.qbPlotter)
# importlib.reload(figPlotter)  

 
plt.ion()
dirnm = os.path.split(qb.__file__)[0]
dirnm = os.path.split(dirnm)[0]
bckDir  = os.path.join(dirnm,'bckUp')
htmlDir = os.path.join(dirnm,'public')

#import pyquaternion as pq
#Rini = pq.Quaternion().random().rotation_matrix
Rini = np.array([[ 0.79495221,  0.43770704, -0.42007563],
       [-0.00645831,  0.69849266,  0.71558807],
       [ 0.60663768, -0.56614534,  0.55809513]])



caseFnm = 'case1.bck'
bckFnm   = os.path.join(bckDir, caseFnm)
htmlFnm  = os.path.join(htmlDir, caseFnm)


cb = qb.cubeSat(bckFnm)

# Set initial attitude (Body to Local frame)
cb.iniSt.R_LB = Rini

# Set Reference states
# 0 RPM at 0s, step to 100RPM at 10000s
cb.refSt.wRW1 = [[0, 0], [100, 10000]] 

# Initialize integrator based on cb.simC
cb.initIntegr()

# Define symbolic system and cythonize it
cb.defSys()
        
# Start integration... and take a nap, seriously..
cb.integr8()

# Sub sample based on dynamics and a regular grid
idx = figPlotter.estSignPnts(cb)

# Disabled png output because it requires plotly-orca..
figPlotter.plotFigs(cb, htmlFnm, show = False, idx = idx)

# Create Animation
import matplotlib.animation 
onOff = ['On','Off']
cb.cbPlot = qb.qbPlotter.qbPlotter()
cb.cbPlot.f.text(0.1, 0.02, 'X', color = 'C0')
cb.cbPlot.f.text(0.11, 0.02, 'Y', color = 'C1')
cb.cbPlot.f.text(0.12, 0.02, 'Z', color = 'C2')
cb.cbPlot.f.text(0.13, 0.02, ', Dashed for Reference,')
cb.cbPlot.f.text(0.32, 0.02, 'Magnetic Field,', color = 'C9')
cb.cbPlot.f.text(0.45, 0.02,'Sun vector',   color = 'C8')
cb.cbPlot.f.text(0.54, 0.02,'(black in shade)')

def animate(frm):
    #k = idx[frm]
    k = frm
    llh = cb.cart2spher(cb.p[:, k])
    title = 'Orbit alignment view at time: {0}(s), Lat: {1:.3f}$^o$,  Lon: {2:.3f}$^o$\n Grav. Gradient:{3},  Aero.:{4},  Control:{5},  RW:{6:.1f}(RPM)'.format(
    cb.t[k], 
    llh[0] * 180 / math.pi,
    llh[1] * 180 / math.pi,
    onOff[cb.enGrvG[k] == 0], 
    onOff[cb.enAero[k] == 0], 
    onOff[cb.enCtrl[k] == 0], 
    cb.wRW1[k])
    sv_LF = cb.R_IL[k].transpose() @ cb.sv[:,k]
    mNrm = np.linalg.norm(cb.magn[:,k])
    cb.cbPlot.updFrame(cb.R_LG[k], sv_LF, cb.magn[:,k] / mNrm, title, cb.isSunVis[k])
animate(0)


plt.plot(cb.t, wNrm, cb.t[idx], wNrm[idx], 'o')

writer = matplotlib.animation.FFMpegWriter(fps = 250, bitrate = 2500)
ani = matplotlib.animation.FuncAnimation(cb.cbPlot.f, animate, frames = len(idx), blit = False);

#with open("myvideo.html", "w") as f:
#    print(ani.to_html5_video(), file = f)
ani.save(htmlFnm[:-3]+'mp4', writer=writer)
ani._stop()
