"""A CubeSat simulator
    
    
    moduleauthor:: Yannis
    
"""

import os
import math
from sympy import *
from sympy.physics.mechanics import *
from sympy.physics.vector import Point, ReferenceFrame
from sympy.physics.vector import Vector, dynamicsymbols
from sympy.utilities.autowrap import autowrap
from sympy import latex
import numpy as np
from numpy import linalg as la
import pyquaternion as pq
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy.integrate
import sys
from qb import qbPlotter
import signal

import time

import sgp4
from sgp4.earth_gravity import wgs72
from sgp4.io import twoline2rv

import datetime
import sciencedates
import pyIGRF

from dataclasses import dataclass, field
from cloudpickle import dumps, loads
import hashlib


@dataclass
class cubeMech:
    """Mechanical properties
    
    """
    #: Cubesat total mass [kg]
    m:    float = 1.98 
    
    #:Cubesat principal inertia, [kg m^2]
    Ib:   np.ndarray = np.diag([0.005e-2, 0.01e-2, 0.01e-2]) 
    
    #: Rotation matrix representing B-frame (principal Inertia) to G-frame
    R_GB: np.ndarray = np.eye(3)
    
    #: Array of Geometry Length, Width, Height [m]
    LWH:  np.ndarray = np.array([0.2, 0.1, 0.1])
    
    #: CoG relative to Geometric center, in Body frame [m]
    CoG_BF: np.ndarray = np.array([0.01, 0, 0]);
    
    #: Drag coefficient [-]
    Cd:   float = 2.0;
    
    # ---------- Derived parameters ----------
    # Area for X,Y,Z faces
    A: np.ndarray = LWH[[1,2,0]] * LWH[[2,0,1]]
         
    # Area vectors for all faces X,Y,Z,-X,-Y,-Z, Body frame 
    A_BF: np.ndarray = np.hstack((np.diag(A),np.diag(-A)))
    
    # PV Panel Area vectors for all faces X,Y,Z,-X,-Y,-Z, Body frame, assume 100 to get percentage 
    APV_BF: np.ndarray = 100 * np.hstack((np.eye(3), -np.eye(3)))
    
    # Pressure point vectors for each face, X,Y,Z,-X,-Y,-Z, Body-frame
    PP_BF: np.ndarray = np.hstack((np.diag(LWH/2), np.diag(-LWH/2)))
    
@dataclass 
class cubeRwMech:
    ''' Reaction wheel parameters  
    
    Attributes:
        mRWa: Reaction wheel mass, [kg]
        IRWa: RW principal inertia, [kg m^2]
    '''
    # Reaction wheel mass, inertia
    mRWa: float = 0.004
    IRWa: np.ndarray = np.array([0.1e-7, 0.1e-7, 2e-7])

@dataclass
class cubeControl:
    ''' Cubesat control parameters 
    
        List of value / transition pairs, e.g [[kp1, t1]] to be used by stepSignal()
        
        Attributes:
        kp: Proportional gain
        kd: Derivative gain, scaled to dt
    '''
    kp: float = field(default_factory=lambda: [[0.0, 0],   [1e-7, 150000]])
    kd: float = field(default_factory=lambda: [[1e-7, 0], [1e-6, 79000]])
    
    
@dataclass
class cubeRwControl:
    ''' Reaction wheel control parameters 
    
    Attributes:
        kp: Proportional gain
        kd: Derivative gain, scaled to dt
        maxTq: Maximum motor's torque to saturate command torque
    '''
    kp: float = 1e-9
    kd: float = 1e-12
    maxTq: float = 0.01e-3
       
@dataclass
class iniOrb:
    ''' Initial orbit parameters 
    
    note: For density 
        See QB50 ADCS Reference Manual v1.0.pdf
        for Coordinates frames p.5/86
        system used: x is  ramair, y normal to orbit, z looks DOWN
        
        see p. 135/198 \"ECSS-E-ST-10-04C_15November2008_.pdf"
        rho ranges from 9.14e-13@380km to 1.63e-10 @200km (6.8e-12 @300km)
        For moderate activity
        rho ranges from 5.60E?12 3@380km to 2.84E?10 @200km (2.52E?11  @300km)
        air drag torque constants
    
    Attributes:
        tle: Two line element orbit
        rho: air density at current height [kg/m^3]
    '''
    #tle:  str = field(default_factory=lambda: ['1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753',
    #             '2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667'])
    # ISS 
    tle:  str = field(default_factory=lambda: ['1 25544U 98067A   20003.38712664  .00000197  00000-0  11608-4 0  9999',
                 '2 25544  51.6449  88.8781 0005239  96.1369   4.4207 15.49530987206314'])
    rho: float = 5.6e-12; 

@dataclass
class iniState:
    ''' Initial state parameters 
    
    Attributes:
    R_LB:    Rotation matrix representing B-frame (principal Inertia) to L-frame (Local orbit frame)
    
    '''
    R_LB: np.ndarray = np.eye(3)
    #R_LB: np.ndarray = pq.Quaternion().random().rotation_matrix
    year:  int = 2019
    month: int = 12
    day:   int = 20
    hour:  int = 8
    min:   int = 24
    sec:   int = 0 
    
@dataclass
class refState:
    ''' Reference state parameters 
    
    Attributes:
    R_LG:    Rotation matrix representing G-frame (geometry frame) to L-frame (Local orbit frame)
    wRW1:    Reaction wheel speed [RPM], list as per stepSignal() input
    
    '''
    
    #R_LG: any = pq.Quaternion().random().rotation_matrix
    R_LG: any = pq.Quaternion([1, 0, 0,0]).rotation_matrix
    wRW1: float = field(default_factory=lambda: [[0, 0], [100, 10000]])

@dataclass
class simCfg:
    ''' Simulation configuration 
    
    Attributes:
    tf:        Final time, [s]
    dt:        Sampling intervals, solver will decide further steps.. [s]
    useCtrl:   Apply Controller, list as per stepSignal() input
    useGrvG:   Apply Gravity Gradient disturbance model, list as per stepSignal() input
    useAero:   Apply Aerodynamic disturbance model, list as per stepSignal() input
    
    '''
    tf: float = 23500 * 10
    dt: float = 1.0
    useCtrl: int = field(default_factory=lambda: [[0, 0], [1, 1001]])
    useGrvG: int = field(default_factory=lambda: [[0, 0], [1, 500]])
    useAero: int = field(default_factory=lambda: [[0, 0], [1, 80001]])

class cubeSat:
    ''' Cubesat model main class 
    
    Attributes:
    tmpDir:    Modules path used to save cython objects. Must have write access otherwise it can be changed
    
    '''
    
    #Earth's standard gravitational parameter
    GM = 3.986005e14 # m3/s2
    DAY2MIN = 24.0 * 60.0
    SEC2MIN = 1 / 60.0
    SEC2DAY = 1 / (24.0 * 60.0 * 60.0)
    
    cbO = iniOrb()

    cbM = cubeMech()
    rwM = cubeRwMech()
    
    cbC = cubeControl()
    rwC = cubeRwControl()
    
    iniSt = iniState()
    refSt = refState()
    
    simC  = simCfg()
    
    tmpDir = os.path.dirname(__file__) + '/cython'
    
    def __init__(self, bckFnm = None):
        print('Init')
        
        sys.path.append(self.tmpDir)
        
        cb = None
        if bckFnm is not None:
            try:
                f = open(bckFnm,'rb')
                cb = loads(f.read())
                f.close()
            except (IOError):
                print('bckFnm / module not found, will use as new back up')
            #except (ModuleNotFoundError):
            #    print('34gt34g')
        
        if cb is not None:
            print('bckFnm loaded, will be replaced for future back ups')
            self.__dict__.update(cb.__dict__)
        else:
            #self.Q_IB = pq.Quaternion()._from_matrix(np.array([[0, 0, 1], [0, 1, 0], [-1.0, 0, 0.0]]))
            # Q representing B-frame to ECI-frame
            self.Q_IB = pq.Quaternion()
            #self.Q_IB = pyquaternion.Quaternion().random()
            
            self.t_prv = 0
                   
            self.orb = twoline2rv(self.cbO.tle[0], self.cbO.tle[1], wgs72)
            
            self.qb = True
            self.cy = True  
            
            self.KM  = None        
            self.isSysDefined = False
            self.isInitialized = False
            self.sysDefMd5 = 0
            self.initMd5 = 0
            self.integrMd5 = 0
            self.cbPlot = qbPlotter.qbPlotter()
        self.bckFnm = bckFnm        
        print('init Inertia:\n',self.cbM.Ib)
        self.signal = None
        self.defSignHndl = signal.getsignal(signal.SIGINT)
        self.keepBckUp()
    
    def keepBckUp(self):
        ok = True
        if self.bckFnm is not None: 
            try:
                with open(self.bckFnm, 'wb') as f:
                    f = open(self.bckFnm, 'wb')
                    KM = self.KM
                    self.KM = None 
                    f.write(dumps(self))
                    f.close()
                    self.KM = KM
            except:
                print('Problem storing back up??')
                ok = False
        return ok
        
    def defSys(self):
        """ Define symbolic system
            
            Mainly used for defining the cython functions MMf()/FMf() for calculating the mass / force matrices
            
        Symbolic matrices stored in MM, FM
        """
        var = dumps([self.cbM, self.rwM])
        md5sm = hashlib.md5()
        md5sm.update(var)
        currMd5 = md5sm.hexdigest()
        
        if self.sysDefMd5 != currMd5 and self.isSysDefined is not True:
            print('Defining new system')
            ''' Defines the symbolic kinematic equations and creates the Cython function'''
            #: Q_IB represents the Body Frame w.r.t ECI frame
            Q_IB = dynamicsymbols('Qb:4')
            
            #: body angular velocity in ECI frame 
            Wbe = dynamicsymbols('Wb:3')
            
            dQ_IB = dynamicsymbols('Qb:4',1)
            mQl = Matrix(Q_IB)
            mdQl = Matrix(dQ_IB)
            #: Angular velocity matrix for the quaternion propagation, w expressed in ECI
            mWbe = Matrix([[0,      -Wbe[0], -Wbe[1], -Wbe[2]],
                           [Wbe[0],       0, -Wbe[2],  Wbe[1]], 
                           [Wbe[1],  Wbe[2],      0 , -Wbe[0]],
                           [Wbe[2], -Wbe[1],  Wbe[0],      0]])
            
            FrECI = ReferenceFrame('FrECI') #: ECI frame
            FrG   = ReferenceFrame('FrG')   #: Geometry frame
            FrB   = ReferenceFrame('FrB')   #: Body frame (aligned to principal axis)
            FrRWa = ReferenceFrame('FrRWa') #: Reaction wheel frame
            
            p  = dynamicsymbols('p:3')      #: Position in ECI, [m]
            dp = dynamicsymbols('p:3',1)
            v  = dynamicsymbols('v:3')      #: Velocity in ECI, [m/s]
            
            m = symbols('m')                #: Cubesat mass [kg]
            I = symbols('I:3')              #: Inertia
                    
            # Reaction Wheel
            qRWa  = dynamicsymbols('qRWa')
            dqRWa = dynamicsymbols('qRWa',1)
            wRWa  = dynamicsymbols('wRWa')
            
            mRWa = symbols('mRWa')
            IRWa = symbols('IRWa:3')
                   
            ut = symbols('ut:3')            #: Input torques (b-frame)
            uf = symbols('uf:3')            #: Input forces  (b-frame, CoM)
            uRWa = symbols('uRWa')
            
            pO = Point('pO')
            pCoM = Point('pCoM')
            #pRW1 = Point('pRW1')
            
            # Initialise frames        
            # FrB is aligned with Principal Inertia Axis
            FrB.orient(FrECI,'Quaternion', Q_IB)
            FrB.set_ang_vel(FrECI, Wbe[0]*FrECI.x + Wbe[1]*FrECI.y + Wbe[2]*FrECI.z)
            #FrB.set_ang_vel(FrECI, Wbe[0]*FrB.x + Wbe[1]*FrB.y + Wbe[2]*FrB.z)
            
            # FrG is aligned with geometry
            FrG.orient(FrB,'Quaternion', pq.Quaternion._from_matrix(self.cbM.R_GB))
            
            # Reaction wheel frame orientation and angular velocity 
            FrRWa.orient(FrG,'Axis', [qRWa, FrG.y])
            FrRWa.set_ang_vel(FrG, wRWa*(FrG.y))
                             
            pO.set_vel(FrECI, 0)
            r = p[0]*FrECI.x + p[1]*FrECI.y + p[2]*FrECI.z 
            pCoM.set_pos(pO,r) 
            pCoM.set_vel(FrECI, v[0]*FrECI.x + v[1]*FrECI.y + v[2]*FrECI.z)
             
            Ib = (inertia(FrB, I[0],I[1],I[2]), pCoM)        
            B    = RigidBody('QB', pCoM, FrB, m, Ib)
            BRw1 = RigidBody('Rw1', pCoM, FrRWa, mRWa, (inertia(FrRWa, IRWa[0],IRWa[1],IRWa[2]), pCoM))
             
            #pRW1.set_pos(pCoM, 0)
            #pRW1.v2pt_theory(pCoM, FrECI, FrB)
             
            q_ind = list(p)
            q_ind.extend(Q_IB)
            q_ind.extend([qRWa])
             
            kin_eq = [dp[i]-v[i] for i in range(3)]
            kin_eq.extend([dqRWa - wRWa])
            #kin_eq.extend(kinematic_equations(Wbe, Q_IB, 'quaternion'))
            kin_eq.extend(list(mdQl-1/2*mWbe*mQl)) 
             
            u_ind = list(v)
            u_ind.extend(Wbe)
            u_ind.extend([wRWa])
             
            Bodies = [B, BRw1]
            # Forces and torques
            W = -(self.GM*(m + mRWa)/r.magnitude()**2) *r.normalize()
            Forces = [(pCoM, W)]
            Forces.extend([(FrB, ut[0]*FrB.x + ut[1]*FrB.y + ut[2]*FrB.z)])
            Forces.extend([(FrB,  -uRWa*FrB.y)])
            Forces.extend([(FrRWa, uRWa*FrB.y)])
            Forces.extend([(pCoM, uf[0]*FrB.x + uf[1]*FrB.y + uf[2]*FrB.z)])
             
            #KM = KanesMethod(FrECI, q_ind=q_ind, u_ind=u_ind, kd_eqs=kin_eq,q_dependent=q_dep, configuration_constraints=c_c)
            KM = KanesMethod(FrECI, q_ind=q_ind, u_ind=u_ind, kd_eqs=kin_eq)
            KM.kanes_equations(Bodies,Forces)
            # todo > gives problems with XXpickle
            self.KM = KM
            
            c_c = Matrix(Q_IB).dot(Q_IB)
            c_cc = {c_c:1}
             
            MM = KM.mass_matrix
            forcing = KM.forcing
            kdd = KM.kindiffdict()
             
            # Symbolic to numerical
            s2n = {m:self.cbM.m, I[0]:self.cbM.Ib[0,0], I[1]:self.cbM.Ib[1,1], I[2]:self.cbM.Ib[2,2]}
            s2n.update({mRWa:self.rwM.mRWa, IRWa[0]:self.rwM.IRWa[0], IRWa[1]:self.rwM.IRWa[1], IRWa[2]:self.rwM.IRWa[2]})
            print(s2n)
             
            # Substitute to mass matrix
            MM = MM.subs(s2n)
             
            # Substitute to force
            forcing = forcing.subs(s2n)
            
            for idx, el in enumerate(MM):
                MM[idx] = el.factor(c_c).subs(c_cc)
             
            for idx, el in enumerate(forcing):
                forcing[idx] = el.subs(kdd)#.factor(c_c).subs(c_cc)
             
            self.MM = MM        #: Symbolic Mass matrix
            self.FM = forcing   #: Symbolic Forcing matrix
             
            # Create a list of [q_ind, u_ind, ub] to pass as input 
            args = flatten([q_ind, u_ind, ut, uRWa, uf])
             
            self.s = symbols('s:{0}'.format(len(args)))
            ds2s = { args[i]:self.s[i] for i in range(len(args))}
            self.args = args    #: Symbolic arguments of the mass / forcing matrix functions
             
            self.MMs = MM.subs(ds2s)        #: Symbolic mass matrix with substituted arguments
            self.FMs = forcing.subs(ds2s)   #: Symbolic forcing matrix with substituted arguments
             
            # Cython functions
            print('Cythonize')
            #: Cython Function evaluating the mass matrix, given numerical values pass by args
            self.MMf = autowrap(self.MMs, backend = "cython",args = self.s, tempdir = self.tmpDir, extra_compile_args =['-O3'])
            #: Cython Function evaluating the forcing matrix, given numerical values pass by args
            self.FMf = autowrap(self.FMs, backend = "cython",args = self.s, tempdir = self.tmpDir, extra_compile_args =['-O3'])
            
            w = symbols('w:3')
    #         v = symbols('v:3')
    #         W   = Matrix([w[0], w[1],  w[2]])
    #         V   = Matrix([v[0], v[1],  v[2]])  
            mW   = Matrix([[   0, -w[2],  w[1]],
                           [ w[2],    0, -w[0]], 
                           [-w[1], w[0],    0]])
            th = sqrt(w[0] * w[0] + w[1] * w[1] + w[2] * w[2])
            mW = mW / th
            R = np.eye(3) + sin(th) * mW + (1 - cos(th)) * mW @ mW
            #self.dth2dRf = autowrap(R, backend="cython", args = w)
            self.isSysDefined = True
        else:
            print('System already defined')
        
        if (self.keepBckUp()):
            self.sysDefMd5 = currMd5
        
    
# to be Cythonized...
    def f(self, t, y):
        """ Integrators function
            Args:
                t:     current time
                y:     state vector
            Returns:
                dy:    state update
        """
        t1 = time.clock()    
        # args -> [p0, p1, p2, Qb0, Qb1, Qb2, Qb3, qRWa, v0, v1, v2, Wb0, Wb1, Wb2, wRWa, ut0, ut1, ut2, uRWa, uf0, uf1, uf2]
        #    y -> [p, qRWa, v, w, wRWa]
        # self.Q_IB.integrate(self.Q_IB.conjugate.rotate(y[7:10]), t - self.t_prv)
        #print(self.Q_IB.q)
        dt = t - self.t_prv
#         if self.qb:
        self.Q_IB.integrate(self.Q_IB.conjugate.rotate(y[7:10]), dt)
        [qw, qx, qy, qz] = self.Q_IB.q
#         else:
#             w = np.array([y[7],y[8],y[9]])
#             w = self.R_IBt.transpose() @ w
#             
#             if self.cy:            
#                 if (dt > 0) & ((w[0] > 0) | (w[1] > 0) | (w[2] > 0)):
#                     dR = self.dth2dRf(w[0] * dt, w[1] * dt, w[2] * dt)
#                 else:
#                     dR = np.eye(3)
#             else:
#                 w = w * dt
#                 dR = dth2Rnu(w)
#             self.R_IBt = self.R_IBt @ dR
#             [qw, qx, qy, qz] = self.R2quat(self.R_IBt)

        #print([qw, qx, qy, qz]) 
        t2 = time.clock()    
        #              [      p0,   p1,   p2,Qb0,Qb1,Qb2,Qb3, qRWa,   v0,   v1,   v2,  Wb0,  Wb1,  Wb2,  wRWa,          ut0,          ut1,          ut2,      uRWa           uf0,          uf1,         uf2]
        self.MMn = self.MMf(y[0], y[1], y[2], qw, qx, qy, qz, y[3], y[4], y[5], y[6], y[7], y[8], y[9], y[10], self.ut_k[0], self.ut_k[1], self.ut_k[2], self.uRW1, self.uf_k[0], self.uf_k[1], self.uf_k[2]) 
        self.FMn = self.FMf(y[0], y[1], y[2], qw, qx, qy, qz, y[3], y[4], y[5], y[6], y[7], y[8], y[9], y[10], self.ut_k[0], self.ut_k[1], self.ut_k[2], self.uRW1, self.uf_k[0], self.uf_k[1], self.uf_k[2])
        
        t3 = time.clock()    
        sol = np.linalg.inv(self.MMn)@self.FMn
        dy = [y[4], y[5], y[6], y[10]]
        dy.extend(sol)        
        self.t_prv = t
        t4 = time.clock()
        self.tic = self.tic + np.array([t2-t1, t3-t2, t4-t3])   
        self.fcalls = self.fcalls + 1 
        #print(t, self.tic/t)
        return dy
    
    def initIntegr(self):
        ''' Initialize state and allocate memory'''
        self.tic = np.zeros(3) 
        self.fcalls = 0
        
        var = dumps([self.cbO, self.iniSt, self.simC])
        md5sm = hashlib.md5()
        md5sm.update(var)
        currMd5 = md5sm.hexdigest()
        
        if self.initMd5 != currMd5:
            tf = self.simC.tf
            dt = self.simC.dt
            # ---------- Allocate memory ----------
            # PVT vectors for every epoch    
            self.t    =  np.arange(0.0, tf, dt)
            self.p    =  np.zeros((3,len(self.t)))
            self.v    =  np.zeros((3,len(self.t)))
            
            # Orbit derived PV from SGP4 for every epoch
            self.orb.p =  np.zeros((3,len(self.t)))
            self.orb.v =  np.zeros((3,len(self.t)))
            
            #: cubesat's angular velocity, ECI frame 
            self.wCbI = np.zeros((3,len(self.t)))
            
            # Roll, pitch, yaw angles
            self.rpy  =  np.zeros((3,len(self.t)))
            
            self.Q    =  np.zeros((4,len(self.t)))
            
            self.R_IB =  np.zeros((len(self.t), 3, 3))  #: Rotation matrix representing Body frame w.r.t ECI, for every epoch 
            
            self.R_IL =  np.zeros((len(self.t), 3, 3))  #: Rotation matrix representing Local Orbit frame w.r.t ECI, for every epoch
            
            self.R_LB =  np.zeros((len(self.t), 3, 3))  #: Rotation matrix representing Body frame w.r.t Local Orbit frame, for every epoch
            
            self.R_LG =  np.zeros((len(self.t), 3, 3))  #: Rotation matrix representing Geometry frame w.r.t Local Orbit frame, for every epoch
            self.aa   =  np.zeros((3,len(self.t)))
    
            self.TCb   = np.zeros((3,len(self.t)))      #: Total Torgue, body frame, for every epoch
            self.FCb   = np.zeros((3,len(self.t)))      #: Total Force, body frame, for every epoch
            self.cTCb  = np.zeros((3,len(self.t)))      #: Controller Torgue, body frame, for every epoch
            self.GgTcb = np.zeros((3,len(self.t)))      #: Gravity gradient Torgue, body frame, for every epoch
            self.AdTcb = np.zeros((3,len(self.t)))      #: Aerodynamic Torgue, body frame, for every epoch
            self.AdFcb = np.zeros((3,len(self.t)))      #: Aerodynamic Force, body frame, for every epoch
            
            self.ut_k  =  np.zeros(3)                   #: Total torgue, body frame, at current epoch
            self.uf_k  =  np.zeros(3)                   #: Total force, body frame @ CoM, at current epoch
                    
            # ---------- Reaction wheel ----------
            self.qRW1  = np.zeros(len(self.t))          #: Angle
            self.wRW1  = np.zeros(len(self.t))          #: Angular velocity
            self.ewRW1 = 0                              #: Angular velocity error
    
            self.solEqvFct  = np.zeros((6,len(self.t)))          #: Solar equivalent area factor, per face
            self.sv         = np.zeros((3,len(self.t)))          #: Sun vector, ECI frame
            self.isSunVis   = np.zeros(len(self.t), dtype =int)  #: Sun visibility flag
            self.magn       = np.zeros((3,len(self.t)))          #: Magnetic line, NED frame
            
            # self.r    =  scipy.integrate.ode(self.f).set_integrator('vode', method='bdf', order=5)
            self.r    =  scipy.integrate.ode(self.f).set_integrator('dopri5', nsteps = 1000, beta = 0.0)
            
            # ---------- Apply Initial parameters ----------
            # Initial Position / Velocity, get it from SGP4
            p, v = self.orb.propagate(self.iniSt.year, self.iniSt.month, self.iniSt.day, self.iniSt.hour, self.iniSt.min, self.iniSt.sec)
            self.p[:,0] = [i*1e3 for i in p]    # [7064e3, 0, 0]
            self.v[:,0] = [i*1e3 for i in v]    # [0,0,8059]
            
            self.orb.p[:,0] = [i*1e3 for i in p]
            self.orb.v[:,0] = [i*1e3 for i in v]
            
            # LOF w.r.t ECI
            self.R_IL[0] = self.getOrbitFrame(self.p[:,0], self.v[:,0])
            
            # Body frame w.r.t ECI 
            self.R_IB[0] = self.R_IL[0] @ self.iniSt.R_LB
            self.R_IBt = self.R_IB[0] 
            self.Q_IB = pq.Quaternion()._from_matrix(self.R_IB[0])
            
            self.jd = sgp4.ext.jday(self.iniSt.year, self.iniSt.month, self.iniSt.day, self.iniSt.hour, self.iniSt.min, self.iniSt.sec)
            self.minFromEpoch = (self.jd - self.orb.jdsatepoch) * self.DAY2MIN
            
            dt = datetime.datetime(self.iniSt.year, self.iniSt.month, self.iniSt.day, self.iniSt.hour, self.iniSt.min, self.iniSt.sec)
            yearI = datetime.datetime(dt.year, 1, 1)
            yearF = yearI.replace(year = dt.year + 1)           
            self.yearFrac = dt.year + ((dt - yearI).total_seconds() / float((yearF - yearI).total_seconds()))
             
            # Convert user input pairs to step signals                      
            self.enGrvG = self.stepSignal(self.simC.useGrvG)
            self.enCtrl = self.stepSignal(self.simC.useCtrl)
            self.enAero = self.stepSignal(self.simC.useAero)
            
            self.cbCkp = self.stepSignal(self.cbC.kp)
            self.cbCkd = self.stepSignal(self.cbC.kd)
            self.refStwRW1 = self.stepSignal(self.refSt.wRW1)
            
            self.wRW1[0] = self.refStwRW1[0] * math.pi / 30
            
            # Vector passed to the integrator's function
            self.y = np.hstack([self.p[:,0], self.qRW1[0], self.v[:,0], self.wCbI[:,0], self.wRW1[0]])
            self.r.set_initial_value(self.y, 0.0)
            
            self.isInitialized = True
            self.initMd5 = currMd5
        else:
            print('Using existing initialization')
        
    def integr8(self):
        """ Main integrator's loop """
        
        var = dumps([self.cbO, self.cbM, self.rwM, self.cbC, self.rwC, self.iniSt, self.refSt])
        md5sm = hashlib.md5()
        md5sm.update(var)
        currMd5 = md5sm.hexdigest()
        
        kini = 0
        if self.integrMd5 == currMd5 and self.isSysDefined and self.isInitialized:
            print('Using back up state from time:', self.k)
            kini = self.k

        self.integrMd5 = currMd5 
        # Catch signal in order to exit cleanly
        signal.signal(signal.SIGINT, self.kbrdIRQhandler)
        tic = time.clock()
        for k, t in enumerate(self.t[kini + 1:-1]):
            k = k + kini
            self.k = k
            
            # Update position from SGP4,             
            p,v = sgp4.propagation.sgp4(self.orb, self.minFromEpoch + t * self.SEC2MIN)
            
            # BFF w.r.t ECI
            if self.qb:
                self.R_IB[k] = self.Q_IB.rotation_matrix
            else:
                self.R_IB[k] = self.R_IBt
            
            # LOF w.r.t ECI
            self.R_IL[k] = self.getOrbitFrame(self.p[:,k], self.v[:,k])
            
            # BFF w.r.t LOF 
            self.R_LB[k] = self.R_IL[k].T @ self.R_IB[k]
            
            # G-frame w.r.t LOF 
            self.R_LG[k] = self.R_LB[k] @self.cbM.R_GB.T 
            
            # roll - pitch - yaw, from body frame to navigation frame
            self.rpy[:,k] =  self.R2xyz(self.R_LG[k])
            
            currJD = self.jd + t * self.SEC2DAY
            # Update Sun vector, currently onle needed for power estimation. Will be used to attitude estimation as well
            self.sv[:,k] = self.sunpos_ECI(currJD)   
            self.isSunVis[k] = (self.p[:, k] @ self.sv[:,k]) > 0  
                     
            lat, lon, h = self.cart2spher(self.p[:, k])
            lon_ECEF = self.eci2ecefLon(lon, currJD)
            
            #Assumes magnetic model is constant during the simulation sine yearFrac is not updated 
            x,y,z,f = pyIGRF.calculate.igrf12syn(self.yearFrac, 2, h / 1000, lat * 180 / math.pi, lon_ECEF * 180 / math.pi)
            
            # Convert NED to ECI, ->, ECI to Local orbit
            self.magn[:,k] = self.R_IL[k].T @ (self.eci2ned(self.p[:, k]).T @ np.array([x, y, z])) 
            
            #self.uRWa = self.motor(10000)
            self.uRW1 = self.motor(1000)
            
            self.cTCb[:,k]  = self.control()
            # Disturbance torques    
            self.GgTcb[:,k] = self.gravGrad(self.p[:,k], self.R_LB[k])
            [self.AdFcb[:,k], self.AdTcb[:,k]] = self.aeroTorq()
            
            self.ut_k  = self.enCtrl[k] * self.cTCb[:,k]
            self.ut_k  = self.ut_k  + self.enGrvG[k] * self.GgTcb[:,k]
            self.ut_k  = self.ut_k  + self.enAero[k] * self.AdTcb[:,k]
            
            self.uf_k = self.enAero[k] * self.AdFcb[:,k]
            
            self.y = self.r.integrate(t)
            
            # Take snapshot
            self.orb.p[:,k+1]   = [i*1e3 for i in p]
            self.orb.v[:,k+1]   = [i*1e3 for i in v]
            
            self.solEqvFct[:, k] = self.estSolarEqvFactor(self.p[:, k], self.sv[:, k], self.R_IB[k])
            
            # y -> p, qRWa, v, w, wRWa
            self.p[:,k+1]  =  self.y[0:3]
            self.qRW1[k+1] =  self.y[3]
            self.v[:,k+1]  =  self.y[4:7]
            self.wCbI[:,k+1] =  self.y[7:10]
            self.wRW1[k+1] =  self.y[10]
            self.TCb[:,k+1] =  self.ut_k # should be i
            self.FCb[:,k+1] =  self.uf_k # should be i
            self.Q[:,k+1]  =  self.Q_IB.q
            #self.cbVis.upd_sat(np.array([0,0,0]), self.R_LB[k])
            if(k % 100 == 0):
                onOff = ['On','Off']
                title = 'Orbit alignment view,\n Time: {0}(s), Grav. Gradient:{1}, Aero.:{2}, Control:{3}, RW:{4:.1f}(RPM)'.format(
                    self.t[k], 
                    onOff[self.enGrvG[k] == 0], 
                    onOff[self.enAero[k] == 0], 
                    onOff[self.enCtrl[k] == 0], 
                    self.wRW1[k])
                sv_LF = self.R_IL[k].transpose() @ self.sv[:,k]
                mNrm = np.linalg.norm(self.magn[:,k])
                self.cbPlot.updFrame(self.R_LG[k], sv_LF, self.magn[:,k] / mNrm, title, self.isSunVis[k])
                ret = self.keepBckUp()
                print(k, self.fcalls, time.clock() - tic,  0.5 * np.linalg.norm(self.cbM.Ib @ (self.wCbI[:,k+1] * self.wCbI[:,k+1])), ret)
                tic = time.clock()
                self.fcalls = 0
            if self.signal != None:
                self.signal = None
                ret = self.keepBckUp()
                print('Back up saved:', ret)
                break
        # Restore signal handler
        signal.signal(signal.SIGINT, self.defSignHndl)        
            #print(i, self.wRWa[i], self.uRWa, self.ub_k)
    def control(self):
        """ Just a high level Cubesat PD control
        
        :note In reality this should drive the coils / motors and the actual torque would have been estimated by those actuators
            
            Returns:
                Command torque 
        """
        k = self.k
        if k > 1:
            # No Attitude estimation, assumes perfect knowledge..
            dR = self.R_LG[k-1] @ self.R_LG[k].T
            w_d = self.R2aa(dR)
            dR = self.refSt.R_LG @ self.R_LG[k].T
            w_p = self.R2aa(dR)
            # control torque in B-Frame 
            u  = self.R_LB[k].T @ (self.cbCkd[k] * w_d + self.cbCkp[k] * w_p)
        else:
            u = np.array([0,0,0.0])
        return u
    
    def motor(self, RPM):
        """ Just a high level PD control
        
        :note if a low level control / motor model is needed, that should be called from within f()
            Args:
                RPM:    Current RW speed [RPM]
            Returns:
                Command torque to the RW flywheel
        """
        e = 2*math.pi*RPM / 60 - self.wRW1[self.k]
        de = e - self.ewRW1
        self.ewRW1 = e
        
        Kd = self.rwC.kd / (self.t[self.k] - self.t[self.k - 1])
        u = self.rwC.kp * e
        # Assume RPM constant or slowly varied
        u = u + Kd * de         
        if abs(u) > self.rwC.maxTq:
            u = self.rwC.maxTq * sign(u)
        return u
    
    def coils(self, v):
        #  Coil model
        print(v)
    

    def gravGrad(self, p, R_LB):
        """ Gravity Gradient Model
        
            "Spacecraft attitude dynamics and control", CDHall
            "Attitude Determination and Control System for AAUSAT3", p. 30
            "Fundamentals of Spacecraft Attitude Determination and Control", p.104
            
            Args:
                p:     position ECI [m]
                R_LB:  Rotation matrix representing Body frame w.r.t Local Orbit Frame
            Returns:
                Gravity gradient torque in Body frame
        """
        r = math.sqrt(p[0]*p[0] + p[1]*p[1] + p[2]*p[2])
        n = R_LB.T @ np.array([0,0,1])  # Express Down vector to Body frame
        gg = self.GM / (r*r*r) * np.cross(n, self.cbM.Ib @ n)
        return gg
    
    
    def aeroTorq(self):
        """ Aerodynamic disturbance Model
            
            Returns:
                Aerodynamic drag and torque in Body frame for current epoch
        """
        k = self.k
        
        # (-) to get medium velocity w.r.t body
        V_IF = -self.v[:,k] 
        # Velocity expressed in Body frame
        V_BF = self.R_IB[k].transpose() @ V_IF        
        # Torque vector due to drag
        M = np.zeros(3);
        # Drag force
        Ft = np.zeros(3);
        
        # Get Force vectors for all faces X,Y,Z,-X,-Y,-Z, Body frame
        F_BF = 0.5 * self.cbM.Cd * self.cbO.rho * self.cbM.A_BF
        for i in range(6):
            tmp = F_BF[:,i] @ V_BF
            if tmp < 0: # Only drag
                F = -tmp * V_BF;
                d = self.cbM.PP_BF[:,i] - self.cbM.CoG_BF;
                M  += np.cross(d,F);
                Ft += F 
                #print(i,self.cbM.A_BF[:,i], tmp, F,d,np.cross(d,F),M)
        return Ft, M
    
    def getOrbitFrame(self, p, v):
        """ Calculate Local Orbit frame from p, v
        
        Orbital frame, Z is down, X is in velocity,  projected to be perpendigular to Z 
        (since non circular orbits), in the same velocity - down plane
        note: 
        Assumes non-zero position, velocity :) 
        Args:
            p: position in ECI [a.u]
            v: velocity in ECI [a.u]
        Returns:
            Rotation matrix of Local Orbit frame w.r.t ECI
        
        """
        R = np.zeros((3,3))
        R[:,2] = -p / np.linalg.norm(p)
        R[:,0] =  v 
        # Fix for non circular orbit's, subtract parralel to Z
        R[:,0] -= np.dot(R[:,0],R[:,2]) * R[:,2]
        R[:,0] /= np.linalg.norm(R[:,0])
        R[:,1] = np.cross(R[:,2], R[:,0])
        return R
    
    def cart2spher(self, p):
        ''' Cartesian to spherical LLH)
        Args: 
            p: vector in cartesian space
        Return:
            Latitude [-90,90], longitude [0, 360], height [same as input]
        '''
        XY = p[0] * p[0] + p[1] * p[1] 
        h = math.sqrt(XY + p[2] * p[2])
        lat = math.atan2(p[2], math.sqrt(XY))
        lon = math.atan2(p[1], p[0])
        return lat, lon, h 
        
    def eci2ecefLon(self, lon, JD):
        ''' ECI longitude to ECEF lon (geocentric)
        Ignoring nutation, etc...
        Args: 
            lon: longitude in ECI
            JD: Julian Date
        Return: 
            ECEF longitude [0, 360]
        '''       
        gst = sgp4.propagation._gstime(JD)
        lon = lon - gst
        if lon < 0:
            lon = lon + 2 * math.pi 
        return lon 
    
    def eci2ned(self, p):
        ''' Construct rotation matrix representing ECI w.r.t NED at location p
        
        Args: 
            lon: longitude in ECI
        Return:
            Rotation matrix converting ECI to NED 
        '''
        x = p[0]
        y = p[1]
        z = p[2] 
        XY = x * x + y * y
        XYZ = np.sqrt(XY + z * z)
        XY = np.sqrt(XY)
        if XY == 0:
            print('NED frame is not defined at poles..')
            return np.eye(3)
        R = np.array([[-x*z/(XY*XYZ), -y*z/(XY*XYZ), XY/XYZ],
                      [        -y/XY,          x/XY,      0],
                      [       -x/XYZ,        -y/XYZ, -z/XYZ]])
        return R
    
    def sunpos_ECI(self, JD):
        """ Calculate Sun position vector in ECI
        
        This algorithm is from
        "Fundamentals of Astrodynamics and Applications", David A. Vallado, 1997, McGraw-Hill,
        Alg. 18, pg.183
        For quick debug use Ex. 3-8, or use an online sun position calculator 
        Args:
            JD:    Julian Date
        Returns:
            Sun position vector in ECI
        """
        T_UT1 = (JD - 2451545.0)/36525
        l_m =  280.4606184 + 36000.77005361*T_UT1  #Mean longitude of the sun
        l_m = math.fmod(l_m,360.0)
        if l_m <0:
            l_m = l_m + 360.0
        T_TDB = T_UT1
        
        man_sun = 357.5277233 + 35999.05034*T_TDB #Mean anomaly of Sun,
        man_sun = math.fmod(man_sun,360.0)
        if man_sun <0:
            man_sun = man_sun + 360.0
        man_sun = man_sun*math.pi/180.0    #in RADIANS..
        
        l_ecl = (l_m + 1.914666471* math.sin(man_sun) + 0.019994643*math.sin(2*man_sun))*math.pi/180.0 #ecliptic longitude of Sun, in RADIANS..
       
        d = 1.000140612 - 0.016708617*math.cos(man_sun) - 0.000139589*math.cos(2*man_sun) #distance to Sun in AUs
        eps = (23.439291 - 0.0130042*T_TDB)*math.pi/180.0 #, in RADIANS..
        r = (math.cos(l_ecl), math.cos(eps)*math.sin(l_ecl), math.sin(eps)*math.sin(l_ecl))
        
        #print JD,T_UT1,l_m,man_sun*180.0/math.pi,l_ecl*180.0/math.pi,d, eps*180.0/math.pi
        #print r
        return np.array(r)
    
    def estSolarEqvFactor(self, p, sv, R_IB):
        ''' Estimates solar equivalent area factor for each face, as a percentage
        
        Returns:
            Factors in [0, 1] to be multiplied by each face's PV area
        
        '''
        tmp = p @ sv
        solEqvFct = np.zeros(6)
        if tmp > 0:
            sv_BF = R_IB.transpose() @ sv
            for i in range(6):
                a = self.cbM.APV_BF[:, i]
                tmp = a @ sv_BF
                if (tmp) > 0:
                    solEqvFct[i] = tmp
        return solEqvFct
    
    def stepSignal(self, c):
        """
        Create step signal from a list of value / transition pairs
        Args: 
            c (list): List of value / transition pairs, e.g [[v1, t1]] 
        
        Returns:
            numpy array: Array with values, v, stepped at t. 
            Length / step follows simulations parameters     
        """
        mxe = c[-1][1]
        c = np.array(c)
        if len(c) > 1:
            c[0:-1, 1] = np.diff(c[:, 1])
        c[-1, 1] = math.ceil(self.simC.tf / self.simC.dt) - mxe
        return np.hstack([np.ones(int(k[1])) * k[0] for k in c])
          
    #"Computing Euler angles from a rotation matrix"
    #http://www.andre-gaschler.com/rotationconverter/
    def R2xyz(self, R):
        eps = 1e-6
        e = np.zeros(3) 
        if (1 + R[0,2] < eps):
            e[0] = -atan2(R[1,0], R[2,0])
            e[1] = -0.5 * math.pi
            e[2] = 0.0
        elif (1 - R[0,2] < eps):
            e[0] = -atan2(-R[1,0], -R[2,0])
            e[1] = 0.5 * math.pi
            e[2] = 0.0;
        else:
            e[0] = -atan2(R[1,2], R[2,2])
            e[1] = asin(R[0,2])
            e[2] = -atan2(R[0,1], R[0,0])
        return e
    def R2aa(self, R):
        return np.array([R[2,1]-R[1,2], R[0,2]-R[2,0], R[1,0]-R[0,1]])
    
    # http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    #@jit(nopython=True)
    def R2quat(self, R):
        tr = R[0, 0] + R[1, 1] + R[2, 2]
        if tr > 0:
            s = sqrt(tr+1.0) * 2; # s=4*qw 
            qw = 0.25 * s;
            qx = (R[2,1] - R[1,2]) / s;
            qy = (R[0,2] - R[2,0]) / s; 
            qz = (R[1,0] - R[0,1]) / s;
        elif (R[0,0] > R[1, 1]) & (R[0,0] > R[2,2]):
            s = sqrt(1.0 + R[0,0] - R[1,1] - R[2,2]) * 2; # s=4*qx 
            qw = (R[2,1] - R[1,2]) / s;
            qx = 0.25 * s;
            qy = (R[0,1] + R[1,0]) / s; 
            qz = (R[0,2] + R[2,0]) / s; 
        elif (R[1,1] > R[2,2]):
            s = sqrt(1.0 + R[1,1] - R[0,0] - R[2,2]) * 2; # s=4*qy
            qw = (R[0,2] - R[2,0]) / s;
            qx = (R[0,1] + R[1,0]) / s; 
            qy = 0.25 * s;
            qz = (R[1,2] + R[2,1]) / s; 
        else: 
            s = sqrt(1.0 + R[2,2] - R[0,0] - R[1,1]) * 2; # s=4*qz
            qw = (R[1,0] - R[0,1]) / s;
            qx = (R[0,2] + R[2,0]) / s;
            qy = (R[1,2] + R[2,1]) / s;
            qz = 0.25 * s;
        return [qw, qx, qy, qz]
    
    def kbrdIRQhandler(self, signal, frame):
        self.signal = signal
        print("Keyboard Interrupt")

def dth2Rp(w0, w1, w2):    
    mW   = np.array([[   0, -w2,  w1], 
                     [ w2,    0, -w0], 
                     [-w1,   w0,  0]])
    th = np.sqrt(w0 * w0 + w1 * w1 + w2 * w2)
    if th > 0.000001:
        R = np.eye(3) + np.sin(th) * mW /th + (mW @ mW) * (1 - np.cos(th)) / th * th
    else:
        R = np.eye(3)
    return R     

#@jit(nopython=True)
def dth2Rnu(w):    
    th = np.linalg.norm(w)
    w = w / th
    mW   = np.array([[   0, -w[2],  w[1]], 
                     [ w[2],    0, -w[0]], 
                     [-w[1],   w[0],  0]])
    if th > 0.000001:
        R = np.eye(3) + np.sin(th) * mW + (mW @ mW) * (1 - np.cos(th))
    else:
        R = np.eye(3)
    return R  

        
